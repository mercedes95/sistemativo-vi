﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.entities;

namespace NETProjectTutorial.dao
{
    interface IDaoFactura : IDao<Factura>
    {
        Factura findById(int id);

        Factura findByCod_Factura(string cod_factura);

        Factura findbyEmpleado(Empleado empleado);
    }
}
