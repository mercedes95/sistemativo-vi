﻿using NETProjectTutorial.dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.entities;
using System.IO;

namespace NETProjectTutorial.implements
{
    class DaoImplementsEmpleado : IDaoEmpleado
    {
        //Header empleado
        private BinaryReader brhempleado;
        private BinaryWriter bwhempleado;

        //Data empleado
        private BinaryReader brdempleado;
        private BinaryWriter bwdempleado;

        private FileStream fshempleado;
        private FileStream fsdempleado;

        private const string FILENAME_HEADER = "hdrempleado.dat";
        private const string FILENAME_DATA = "dtempleado.dat";
        private const int SIZE = 390;

        public DaoImplementsEmpleado() { }

        private void open()
        {
            try
            {
                fsdempleado = new FileStream(FILENAME_DATA, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                if (!File.Exists(FILENAME_HEADER))
                {
                    fshempleado = new FileStream(FILENAME_HEADER, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhempleado =  new BinaryReader(fshempleado);
                    bwdempleado = new BinaryWriter(fshempleado);

                    brdempleado = new BinaryReader(fsdempleado);
                    bwdempleado = new BinaryWriter(fsdempleado);

                    bwhempleado.BaseStream.Seek(0, SeekOrigin.Begin);
                    bwhempleado.Write(0);
                    bwhempleado.Write(0);

                }
                else
                {
                    fshempleado = new FileStream(FILENAME_HEADER, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    brhempleado = new BinaryReader(fshempleado);
                    bwhempleado = new BinaryWriter(fshempleado);
                    brdempleado = new BinaryReader(fsdempleado);
                    bwdempleado = new BinaryWriter(fsdempleado);
                }
            }catch(IOException e) {
                throw new IOException(e.Message);
            }
        }

        private void close()
        {
            try
            {
                if(brdempleado != null)
                {
                    brdempleado.Close();
                }
                if(brhempleado != null)
                {
                    brhempleado.Close();
                }
                if(bwdempleado != null)
                {
                    bwdempleado.Close();
                }
                if(bwhempleado != null)
                {
                    bwhempleado.Close();
                }
                if(fsdempleado != null)
                {
                    fsdempleado.Close();
                }
                if(fshempleado != null)
                {
                    fshempleado.Close();
                }
            }catch(IOException e) {
                throw new IOException(e.Message);
            }
        }

        public bool delete(Empleado t)
        {
            throw new NotImplementedException();
        }

        public List<Empleado> findAll()
        {
            open();
            List<Empleado> empleado = new List<Empleado>();
            brhempleado.BaseStream.Seek(0, SeekOrigin.Begin);
            int b = brhempleado.ReadInt32();
            for( int i = 0;  i< b; i++)
            {
                //Se calcula la posicion cabecera
                long hpos = 8 + i * 4;
                brhempleado.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brhempleado.ReadInt32();
                //Se calcula los datos
                long dpos = (index - 1) * SIZE;
                brdempleado.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdempleado.ReadInt32();
                string cedula = brdempleado.ReadString();
                string nombre = brdempleado.ReadString();
                string apellidos = brdempleado.ReadString();
                double salario = brdempleado.ReadDouble();
                string tconvencional= brdempleado.ReadString();
                string tcelular = brdempleado.ReadString();
                string direccion = brdempleado.ReadString();
                
                Empleado e = new Empleado(id, cedula, nombre, apellidos, salario, tconvencional, tcelular, direccion );
                empleado.Add(e);
            }

            close();
            return empleado;

        }

        public Empleado findByCedula(string cedula)
        {
            throw new NotImplementedException();
        }

        public Empleado findById(int id)
        {
            throw new NotImplementedException();
        }

        public List<Empleado> findByLastname(string lastname)
        {
            throw new NotImplementedException();
        }

        public void save(Empleado t)
        {
            open();
            brhempleado.BaseStream.Seek(0, SeekOrigin.Begin);
            int m = brhempleado.ReadInt32();
            int n = brhempleado.ReadInt32();

            long dpos = n * SIZE;
            bwdempleado.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdempleado.Write(++n);
            bwdempleado.Write(t.Cedula);
            bwdempleado.Write(t.Nombre);
            bwdempleado.Write(t.Apellidos);
            bwdempleado.Write(t.Salario);
            bwdempleado.Write(t.Inss);
            bwdempleado.Write(t.Tconvencional);
            bwdempleado.Write(t.Tcelular);
            bwdempleado.Write(t.Direccion);

            bwhempleado.BaseStream.Seek(0, SeekOrigin.Begin);
            bwhempleado.Write(++m);
            bwhempleado.Write(n);

            long hpos = 8 + (n - 1) * 4;
            bwhempleado.BaseStream.Seek(hpos, SeekOrigin.Begin);
            bwhempleado.Write(n);
            close();
        }

        public int update(Empleado t)
        {
            throw new NotImplementedException();
        }
    }
}
