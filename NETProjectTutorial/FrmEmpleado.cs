﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmEmpleado : Form
    {
        private DataTable tblEmpleados;
        private DataSet dsEmpleados;
        private BindingSource bsEmpleados;
        private DataRow drEmpleados;

        public FrmEmpleado()
        {
            InitializeComponent();
            bsEmpleados = new BindingSource();
        }

        public FrmEmpleado(DataTable tblEmpleados, DataSet dsEmpleados)
        {
            this.TblEmpleados = tblEmpleados;
            this.DsEmpleados = dsEmpleados;
        }

        public DataRow DrEmpleados
        {
            set
            {
                drEmpleados = value;
                txtINSS.Text = drEmpleados["INSS"].ToString();
                txtCed.Text = drEmpleados["Cédula"].ToString();
                txtNombres.Text = drEmpleados["Nombres"].ToString();
                txtApellidos.Text = drEmpleados["Apellidos"].ToString();
                txtDireccion.Text = drEmpleados["Dirección"].ToString();
                txtConvencional.Text = drEmpleados["Teléfono"].ToString();
                txtCelular.Text = drEmpleados["Celular"].ToString();
                txtDireccion.Text = drEmpleados["Dirección"].ToString();
                cmbSexo.SelectedItem = drEmpleados["Sexo"].ToString();
            }
        }

        public DataTable TblEmpleados
        {
            get
            {
                return tblEmpleados;
            }

            set
            {
                tblEmpleados = value;
            }
        }

        public DataSet DsEmpleados
        {
            get
            {
                return dsEmpleados;
            }

            set
            {
                dsEmpleados = value;
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            string cedula, inss, nombres, apellidos, tConvencional, direccion, tCelular, sexo;
            double salario;

            inss = txtINSS.Text;
            cedula = txtCed.Text;
            nombres = txtNombres.Text;
            apellidos = txtApellidos.Text;
            direccion = txtDireccion.Text;
            tConvencional = txtConvencional.Text;
            tCelular = txtCelular.Text;
            salario = double.Parse(txtSalario.Text);
            sexo = cmbSexo.SelectedItem.ToString();
            

            if (drEmpleados != null)
            {
                DataRow drNew = TblEmpleados.NewRow();

                int index = TblEmpleados.Rows.IndexOf(drEmpleados);
                drNew["Id"] = drEmpleados["Id"];
                drNew["INSS"] = inss;
                drNew["Cedula"] = cedula;
                drNew["Nombres"] = nombres;
                drNew["Apellidos"] = apellidos;
                drNew["Direccion"] = direccion;
                drNew["Convencional"] = tConvencional;
                drNew["Celular"] = tCelular;
                drNew["Salario"] = salario;
                drNew["Sexo"] = sexo;


                TblEmpleados.Rows.RemoveAt(index);
                TblEmpleados.Rows.InsertAt(drNew, index);
                TblEmpleados.Rows[index].SetModified();

            }
            else
            {
                TblEmpleados.Rows.Add(TblEmpleados.Rows.Count + 1, inss, cedula, nombres, apellidos, direccion, tConvencional, tCelular, salario, sexo);
            }

            Dispose();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void FrmEmpleado_Load(object sender, EventArgs e)
        {
            bsEmpleados.DataSource = DsEmpleados;
            bsEmpleados.DataMember = DsEmpleados.Tables["Empleado"].TableName;
        }
    }
}
